﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameBackend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBackend.Tests
{
    [TestClass()]
    public class BallTests
    {
        [TestMethod()]
        public void BallTest()
        {
            Ball ball = new Ball(1, 2);
            Assert.IsNotNull(ball);
        }

        [TestMethod()]
        public void MoveTest()
        {
            Ball ball = new Ball(1, 2);
            ball.Move();
            Assert.AreEqual(ball.coordinate.x, 2);
        }
    }
}