﻿namespace GameBackend;
public class Ball
{
    public Coordinate coordinate= new Coordinate();
    public float Width { get; }
    public float Height { get; }

    private float _screenWidth;
    private float _screenHeight;

    public Vector Velocity { get; set; } = new Vector(1, 1);

    public Ball(int x, int y, float width, float height, float screenWidth, float screenHeight)
    {
        coordinate.x = x;
        coordinate.y = y;
        Width = width;
        Height = height;
        _screenWidth = screenWidth;
        _screenHeight = screenHeight;
    }
    public void Move()
    {
        if (coordinate.x + Width <= 0 || coordinate.x + Width > _screenWidth) {

            coordinate.x -= Velocity.Vx;
        }
        else
        {
            coordinate.x += Velocity.Vx;
        }
        if (coordinate.y + Height <= 0 || coordinate.y + Height > _screenHeight)
        {

            coordinate.y -= Velocity.Vy;
        }
        else
        {
            coordinate.y += Velocity.Vy;
        }

    }
}
public class Vector
{
    public float Vx { get; set; }
    public float Vy { get; set; }

    public Vector(float vx, float vy)
    {
        Vx = vx;
        Vy = vy;
    }
}
public class Coordinate
{
    public float x { get; set; }
    public float y { get; set; }
    public Coordinate()
    {
        x = 0;
        y = 0;
    }
}
