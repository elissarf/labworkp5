﻿using GameBackend;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BouncingBallGame
{
    internal class BallSprite : DrawableGameComponent
    {
        private Ball _ball;
        private SpriteBatch _spriteBatch;
        private Texture2D _ballImage;
        private Game _game;

        public BallSprite(Game game) : base(game)
        {
            _game= game;
            

        }
        public override void Initialize()
        {
            base.Initialize();
            _ball = new Ball(0, 0, _ballImage.Width, _ballImage.Height, GraphicsDevice.Viewport.Width,
                GraphicsDevice.Viewport.Height);
        }
        protected override void LoadContent()
        {
            base.LoadContent();
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            _ballImage = _game.Content.Load<Texture2D>("redball");

        }
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            _spriteBatch.Begin();

            _spriteBatch.Draw(_ballImage, 
                new Vector2(_ball.coordinate.x, _ball.coordinate.y), Color.White);

            _spriteBatch.End();
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            _ball.Move();
        }
    }
}
